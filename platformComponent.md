# Copy from nmc-6.1

Create a folder `src/platform`

Copy `PlatformComponent.js` into the folder

Save the file

# Copyright

Remove copyright, it can be added to `base.webpack.config.js` using `BannerPlugin`

```javascript
  const webpack = require("webpack");

  [...]

  plugins: [
    new webpack.BannerPlugin(
      {
        "banner": "Copyright © 2018 Nuance Communications Inc. All rights reserved.",
        "include": /src\/**.*.js/
    ),
```

# platform constants

see `platformConstants.md`

# platformComponent.js

`PlatformComponent.js` is a self executing function and will be changed to a module.

Click `spaces: 4` in the status bar and change to 2.

Remove the first and last lines in the file.

Ctrl+A, Shift+tab to correct indent

# disable linting

TEMPORARILY disable the jsdoc and eqeqeq rules by adding a comment at the top of the file.

```javascript
/* eslint no-redeclare: 0 */
/* eslint no-undef: 0 */
/* eslint quotes: 0 */
/* eslint curly: 0 */
/* eslint no-unused-vars: 0 */
/* eslint require-jsdoc: 0 */
/* eslint valid-jsdoc: 0 */
/* eslint jsdoc/require-description: 0 */
/* eslint eqeqeq: 0 */
```

These can be removed one at a time once it is confirmed the component loads.

# initial format

Save the file so that it is formatted.

# NMCApp

Add an import `import NMCApp from "./../nmc/NMCApp.js";`

# SysErr

Add an import `import SysErr from "./../nmc/SysErr.js";`

# nmc.platform functions

Convert `nmc.platform.[name] = function` to `export function [name]`

This is preferable because it is immediately obvious what is publicly available.

The alternative is to add an `export { name }` at the end of the file.

# nmc.platform.OrganizationControl

Change `new nmc.platform.OrganizationControl` to `new OrganizationControl`

# nmc.platfrom.strings

add `import i18n from "i18next";`

Change all instances of `nmc.platfrom.strings.` to `i18n.t("platform:`

Hint: find/replace with those values then find `i18n.t` and end with `")`. Press F3 to find next.

# nmc.platform.[enum_type]

add `import platformConstants from "./platformConstants.js";`

Find references to the platform constants `nmc.platform.[enum_type]` and change to `platformConstants.[enum_type]`

# Import the plugin

change App.js to require the plugin.

```javascript
    axios
      .get("SessionInfo")
      .then(json => this.startSession(json.data))
      .then(() => {
        this.loadPlugins();
        NMCApp.initUi();
      });
  }

  /**
   * @description Load the plugins.
   *
   * @returns {undefined} Nothing.
   */
  loadPlugins() {
    require("./platform/PlatformComponent.js");
  }

```

Note that the plugin must be loaded before initUi is invoked.

# isAllOrganizationDefault

This fails because `isAllOrganizationDefault` is now a variable and a function.

Delete the function and export the variable at the end of the file.

```javascript
export { isAllOrganizationDefault };
```

# manageOrganization

This is also duplicated, but as functions. Rename the `internal one` as `privateManageOrganization` and leave the other as

```javascript
export function manageOrganization(...
```

There are two invocations of it that will need to be changed.

# setNMCSessionInfo

The invocation of `NMCApp.setNMCSessionInfo` in `App.js` still has `@(session` artifacts in it.

It should be updated to use the session object returned from the `data-server`.

```javascript
NMCApp.setNMCSessionInfo(
  session.sessionToken,
  session.login,
  session.lastName,
  session.firstName,
  session.userUID,
  session.organizationUID,
  session.organizationName,
  session.institutionCode,
  session.defaultOrganizationUID,
  session.isMultiAccountViewable,
  session.useSpokenFormExtendedChars,
  session.isDevelopmentPartner,
  session.previleges,
  session.globalGrants,
  session.generalizedGrantCapabilities,
  session.nmsDeploymentMode
```

# NMCApp.event

import { event as nmcEvent } from "./../nmc/nmcConstants.js";

Change all instances of NMCApp.eventTypes to nmcEvent

# nmc.platform.strings in NMCApp.js

Add an import to `NMCApp.js` and change all references of `nmc.platform.strings` to `i18n.t("platform:`

```javascript
import i18n from "i18next";
```

# nmc.platform in NMCApp.js

Ignoring the cyclical implications for now...

Add an import to `NMCApp.js` and change all references of `nmc.platform` to `platformConstants`

```javascript
import * as platformConstants from "../platform//platformConstants.js";
```

# resolve.alias

There are now imports for `./../nmc/[foo]`. A webpack alias can be defined to prevent the need to know the relative path.

Add a section to `base.webpack.config.js` after `output` and before `watchOptions`

```javascript
  resolve: {
    alias: {
      nmc: path.resolve(__dirname, "src/nmc/")
    }
  },
```

Now `import "./../nmc/foo.js"` can be coded as `import nmc from "nmc"`

# CancelEventArgs

Note that now NMCApp has three remaining references to the nmc namespace, for `nmc.platform.CancelEventArgs`

This is declared in `PlatformComponent.js`. If we were to import that into `NMCApp.js` it would introduce a cyclical dependency.

TODO: Discuss with the team that this should be a class in the nmc folder.

see `cancelEventArgs.md`

Add `import CancelEventArgs from "nmc/CancelEventArgs.js";`

# virtualDirectoryName_Ex

`import { virtualDirectoryName_Ex } from "nmc/nmcUtils.js";`

Change `NMCApp.getVirtualDirectory_Ex()` to `virtualDirectoryName_Ex`

TODO: Time to revisit virtual directory?

# Warnings

Look at the yellow warnings in the build window

`import * as dialogs from "nmc/dialogs.js";`

change `NMCApp.showNMCConfirmWindow` to `dialogs.showConfirmWindow`
change `NMCApp.showNMCExceptionWindow` to `dialogs.show`
change `NMCApp.showNMCInformationWindow` to `dialogs.showInformationWindow`

# uuid

`import uuid from "nmc/uuid.js";`
change `NMCApp.uuid` to `uuid`

# ../images

TODO: Confirm based on previous discussions.

Change `"../images` to `"images`

# virtualDirectoryName_Ex

Add `import { virtualDirectoryName_Ex } from "./../nmc/nmcUtil.js";`

Change instances of `NMCApp.getVirtualDirectoryName_Ex()` to `virtualDirectoryName_Ex`

# functions

Change `nmc.platform.foo = function` to `export function foo`

# PROBLEMS

Click the problems tab and work through them, ignoring unused variables for now.

# lint errors

See `PlatformComponentLint.md`

# variable declarations.

Typically, variable declarations are at the top, so move the declaration for `getOrganizations`

Move any vars declared mid-module to the top. (They get hoisted to the top anyway)

Change those `var` declarations to `let`

## self

TODO: There is a `var self` declared in function `ApplicationSection` and assigned a value of `this` in multiple places. This can be deleted?

# Function and logic blocks

Move the function definitions after any inline logic.

TODO: If the intention is to keep the code with the function then consider encapsulating the inline code in functions and having a set of invocations at the top.

It would probably be easier to read if the inline code was broken into functions anyway with a single invocation at the top, e.g. initialize();

# Unused variables

TODO: There are several unused variables. A search of nmc-6.1 finds they are not used. Are they?

# eqeqeq

TODO: Each instance of == or != must be examined for coercion.

# 404 Image not found

Copy the images from nmc project to NMCHTML folder.
