# data-server

There is code in `NMC/Views/Home/Main.cshtml` which invokes `NMCApp.Init` and `NMCApp.setNMCSessionInfo` with data gleaned from the server.

This data is now provided by `SessionInfo.js`.

The data will be retrieved from the server using the login credentials.

# proxy

Since the client is accessed using a port and the server runs on a different port we may run into CORS issues. The server could be configured to accept CORS but we would still need to code the port.

In production this is not neccessary.

To emulate that environment we use a proxy.

Authentication in the development environment is provided using basic auth.

A parameter of `users=users.json` is passed in the `start.bat` file in the `nmc-proxy` project.

Use `Terminal > Run Task... > Start proxy (nmc-proxy)`

# Retrieve session info

The session info is provided by the `data-server` project.

Select `Terminal > Run Task... > Start data-server (nmc-data-server)`

To confirm this works enter http://localhost:4013/NMCHTML/SessionInfo into the browser, you should see 'Unauthorzied"
