At this stage we should have both the NMC application scaffolding and the platform plugin working, albeit with some minor issues...

# linting

Remove the eslint overides at the top of the file, one by one, and fix anny issues.

# no-redeclare

var headerId = header_Id;
var containerId = container_Id;

# no-undef

`buttonEventBus` should be `applicationTabEventBus` ??

Typo? `liceseType`

Note: This shows the benefits of linting and the IDE. These are some potentially nasty bugs to fix.

# quotes

# sanity check

Any yellow warnings in the build?

Is the UI still building?

Any errors in the DEBUG CONSOLE tab?

```
404 Diagnostics/SaveClientPlatformInfo
404 PlatformUtilities/GetMessageCount
```

These cause a `TypeError: Cannot read property 'toLowerCase' of undefined` in jquery because the server returns "Not Found".

## Update the data-server

Add the above paths to `routes.js` and create a `.js` files for each

### SaveClientPlatformInfo.js

```javascript
let express = require("express");
let router = express.Router();

router.post("/", function(req, res) {
  res.json("Not implemented");
});

module.exports = router;
```

### GetMessageCount.js

```javascript
let express = require("express");
let router = express.Router();

router.get("/", function(req, res) {
  res.json("0");
});

module.exports = router;
```

## Missing images

Ignore them for now.

# curly

Remove `/* eslint curly: 0 */`

Fix errors in PROBLEMS tab

check DEBUG CONSOLE

check build output

# unused var

Remove `/* eslint no-unused-vars: 0 */`

Fix errors in PROBLEMS tab

check DEBUG CONSOLE

check build output

# jsdoc

Remove the three comments for jsdoc

Fix errors in PROBLEMS tab

check DEBUG CONSOLE

check build output

# comments

change `/**********` to `/* *******`, otherwise it will be treated as jsdoc

Fix errors in PROBLEMS tab

check DEBUG CONSOLE

check build output

# PHEW!

Make sure it still renders :)
