Note: The tab size may be calculated incorrectly. Click the Spaces entry in the IDE status bar and select `spaces; configured value`

Open `main.js` and save to format it.

# JSDoc

N.B. typo in comment at top.

1. Address all jsdoc errors.
2. Address braces required for if logic
3. Address eqeqeq
4. Address function never used

   ```javascript
   export = {
     onBeforeUnloadNmcMainHandler,
     onUnloadNmcMainHandler,
     checkBrowerInfo
   };
   ```

   TODO: The typo checkBrow(s)erInfo

# i18n

There are two strings accessed using `nmc.framework...`

Internationalization is provided by `i18Next`, so add an import

```javascript
import * as i18n from "i18next";
```

This requires the string be declared in the locale files.

1. Rename the `t.json` file to `framework.json` in both the `en` and `fr` folders.
   Copy the item `NotSupportedBrowserWarningMsg` from `NMCFrameworkStrings.js` into the file.

   Do the same for `fr`.

   Change the code in main.js

   ```javascript
   alert(i18n.t("framework:NotSupportedBrowserWarningMsg"));
   ```

2. LeaveNMCPageWithoutLoggingOutWarningMsg

   This is a value in `platform.strings` which requires that `PlatformStrings.js` is loaded. This would not seem to be the best place since the application scaffolding now has a dependency on the platform plugin.

   The value is defined in NMC.Plugin.Platform\Scripts. Copy it into the framework locale files.

   change the code in main.js

   ```javascript
   return i18n.t("framework:LeaveNMCPageWithoutLoggingOutWarningMsg");
   ```

   TODO: Discuss with team.

3. Change the code in i18nOptions to load the new file.

```javascript
  ns: ["framework"],
  backend: { loadPath: "locales/{{lng}}/{{ns}}.json", crossDomain: true },
```

# jQuery

jQuery is accessed using the global variable `$`. This would cause an eslint error `$ is not defined`

`jquery: true` has been added to the `env` item of the `export` section in `.eslintrc.js` to avoid this.

# getVirtualDirectoryName_Ex

This method is invoked in many places and is probably not suited to be defined in NMCApp since we do not want to import that everywhere.

See `nmcUtils.md`

Add an import into `main.js` for the util

```javascript
import { virtualDirectoryName_EX } from "./nmc/nmcUtils.js";
```

Replace the code in main.js with `virtualDirectoryName_EX`

# ESLint string must use double-qutes

```javascript
// prettier-ignore
"input[name=\"__RequestVerificationToken\"]";;

```

# One last error

That leaves an eqeqeq error: version\/(\d+)/i)) `!=` null
