# NMCApp

## namespace

There are two functions dealing with namespace that can be abstracted to a file.

# isMenuSectionVisible

`isMenuSectionVisible` is defined as a global in `NMCApp.js`

`NMCApp.js` constructor invokes `new RibbonControl`

`RibbonControl::addMenu` invokes `new Menu`

`Menu` constructor sets `isMenuSectionVisible` to `true`

`Menu::hideShowMenuSection(showMenu)` sets `isMenuSectionVisible` to `showMenu` if not already equal.
