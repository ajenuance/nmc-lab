Check out the `nmc-plugin-platform` project.

# Create locale files and folders

- Create a folder named `locales` in the root of the project
  Add two sub-folders named `fr` and `en`
- Copy `PlatformStrings` and `PlatformString_fr.js` to `platfrom.json` in each of the locale folders.

# Convert to json

For each file:

- Change spaces to 2 (Click the word `Spaces` in the bottom right of the blue gutter)
- Change the content to json
- Resolve problems in the PROBLEMS tab

# Sort

- Use the sort plugin to sort the file.
- Remove empty lines.
- Remove the comments.
- Resolve duplicates.

# Notes

1. The `last` definition wins.
2. Who translated? ... Status is not Etat.
3. The name of the keys should reflectwhat the content is, as much as possible, and not why or where it is used.
   so, what is `NPI:RPPS`?

# Initializing i18n

Add logic into `PlatformComponent.js` to load the namespace. Since this is a plugin for the NMCApp it is safe to assumes `i18next` has already been configured by the app.

Add the following code after the imports:

```javascript
i18n.loadNamespaces("platform", (err, t) => {
  /* resources have been loaded */
});
```

The string value specified is the name of the file created in the `locales` sub-folders.

Note:

If there were code in `PlatformComponents.js` that needed to be run when the file is `required/imported` it must be run from the (empty) function provided to the invocation of `loadNamespaces`.

# Build with webpack

## package.json

Add the following after the version entry:

```json
  "author": "Anthony Elcocks",
  "license": "ISC",
  "repository": {
    "type": "git",
    "url": "git://localhost:4873/username/repository.git"
  },
  "scripts": {
    "clean": "rimraf dist/*",
    "build": "cross-env npm run clean && webpack -d",
    "pub": "cross-env npm version patch && npm publish"
  },
  "files": [
    "dist"
  ],

```

Change the browser entry to `"browser": "./dist/PlatformComponent.js",`

## Install modules for the build

```
npm install --save-dev copy-webpack-plugin cross-env i18next rimraf webpack webpack-cli
```

# Convert assets to constants

```javascript
export let images = Object.freeze({
  acct_add: "images/32x32_acct_add.png",
  [...]
```

# webpack.config.js

```javascript
const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: "./index.html",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.html"
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: "*.js",
        to: "[name].[ext]",
        ignore: ["webpack*"]
      },
      {
        from: "images",
        to: "images"
      },
      {
        from: "locales",
        to: "locales"
      }
    ])
  ]
};
```

# gitignore

Add a `.gitignore` file in the project root.

```
node_modules
dist
```

# Build the project

```
npm run build
```

# Update version and deploy

Commit changes to git, otherwise the version upgrade will fail.

Run verdaccio from the task menu.

Open a terminal to the `nmc-plugin-platform` project.

```
npm run pub
```

This increases the patch version and deploys.

TODO: Perhaps better to have three scripts? PubPatch, PubMinor, PubMajor ?

# Update client to use the 'new' Platform plugin

## webpack.config.js

Copy the assets from the plugin to the output directory of the client

Add the following after the existing `locales` entry for `CopyWebpackPlugin`

```javascript
,
{
  from: "node_modules/@nmc/nmc-plugin-platform/dist/images",
  to: "images"
},
{
  from: "node_modules/@nmc/nmc-plugin-platform/dist/locales",
  to: "locales"
}
```

## locale files

Add two entries to the locale files:

locales/en/framework.json

```json
"UnsavedInformationCaption": "Unsaved Information",
"UnsavedInformationMsg": "There is information that has not been saved.  Do you want to lose your changes and log out?",
```

locales/fr/framework.json

```json
"UnsavedInformationCaption": "Informations non enregistrées",
"UnsavedInformationMsg": "Certaines informations n'ont pas été enregistrées. Si vous vous déconnectez, les informations seront perdues. Voulez-vous vraiment vous déconnecter?",
```

## app.js

Change the `loadPlugins` function to load the plugin from the dist folder.

```javascript
require("@nmc/nmc-plugin-platform/dist/PlatformComponent.js");
```

## NMCApp.js

Update the import for the plugin

```javascript
import * as platformConstants from "@nmc/nmc-plugin-platform/dist/platformConstants";
```

## Update the platform plugin version

Open a terminal to the `nmc-client` project

```
npm update @nmc/nmc-plugin-platform
```
