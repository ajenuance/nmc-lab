# Create the file

1. Create a folder named `nmc` under the `src` folder

2. Create a file named `nmcUtils.js`

# Virtual directory

copy the `getVirtualDirectoryName_Ex` function from `NMCApp` to `nmcUtils.js`

add the code to `nmcUtils.js`

```javascript
let virtualDir;

const virtualDirectory = location.pathname.slice(
  1,
  location.pathname.indexOf("/", 1)
);
const virtualDirectory_Ex = typeof virtualDir === "undefined" ? "" : virtualDir;
/**
 * @description Set the path prefix used to access the application.
 *
 * @param {string} path - The path prefix.
 *
 * @returns {undefined} Nothing.
 */
function setVirtualDirectory(path) {
  virtualDir = path;
}

export { virtualDirectory, virtualDirectory_Ex, setVirtualDirectory };
```

# Initialization

virtualDir is passed as a parameter to the `publicInit` method of `NMCApp.js`.

TODO:

- This is still `@ViewData` and needs to be addressed.
- `NMCApp` must pass this through to `nmcUtils`.
- There is commented code in the original. Why not use that?
- Discuss relative addressing with the team.
