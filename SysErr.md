TODO: Is this miss-named? Isn't it really SysLog?

# Format the file

Once `SysErr.js` has beem moved into `src/nmc`, open and format the file.

Win: Shift + Alt + F

Note: There may be tabs in the file. Click the `spaces` entry in the status bar and select `Indent using spaces > Configured tab size`.

# Converto to a module.

This is a module now, so remove the instantiaion code at the top of the file

```javascript
var SysErr = SysErr || new Logging();
```

# esLint

Then correct linting errors (Hint: switch to the PROBLEMS tab)

Ignore the errors for `NMCApp` until the end.

# Quotes

There are several instances where eslint complains about the use of quotes:

```javascript
'input[name="__RequestVerificationToken"]';
```

However, prettier will correct any changes so use:

```javascript
// prettier-ignore
"input[name=\"__RequestVerificationToken\"]";;

```

# LoggedItem

The loggedItem has c# comments. Move them up using Shit + Alt.

The comments use a custom object type, `LoggingMessageType`. Use @typedef

The return type is actually undefined. If we invoke `let foo = LoggedItem();`

```javascript
/**
 * @typedef {Object} LoggingMessageType
 *
 * @description Constructor for LoggedItem object.
 *
 * @param {string} message - The message being logged.
 * @param {LoggingMessageType} type - The type of message logged (Info or Error).
 * @param {Object} details - Optional object containing additional details.
 *
 * @returns {undefined} Nothing.
 */
```

# NMCApp missing

Add an import statement for the `nmcUtils.js` file recently created.

```javascript
import nmcUtils from "./nmcUtils.js";
```

Replace invocations of `NMCApp.getVirtualDirectoryName_Ex()` with `nmcUtils.virtualDirectoryName_EX`

# uploadAlert

SysErr invokes `NMCApp.uploadAlert`.

1. Open `NMCApp.js` and find `function uploadAlert`

2. Copy the code into `nmcUtils.js` as `uploadAlert` and add it to the `export` section.

3. Correct errors
   Note: Use `this.virtualDirectory_Ex`

TODO: Remove `uploadAlert` and `handleNMCException` from `NMCApp.js` using `nmcUtils.js` instead.

```javascript
// Notice how there are compilation errors in the app server window. This updates with each save.
```

# handleNMCException

nmcUtils now invokes handleNMCException.

Copy the function from `NMCApp.js` to `nmcUtils.js` and correct errors.

This invokes functions defined in NMCApp.js to show dialogs.

Create a file named `dialogs.js` in `src/nmc`. For the moment it will be 'empty'

```javascript
/**
 * @description Show an error dialog.
 *
 * @param {string} error - Custom error information.
 *
 * @returns {undefined} Nothing.
 */
function showErrorWindow(error) {}

/**
 * @description Show an exception dialog.
 *
 * @param {*} error - Exception error information. This can be either an Error object or string.
 *
 * @returns {undefined} Nothing.
 */
function showExceptionWindow(error) {}

export { showErrorWindow, showExceptionWindow };
```

import the file into `nmcUtils`

Change the code to invoke the methods.

There should no longer be compilation errors.

# Modularize

Now we must turn the file into a JavaScript module.

Remove the function 'Logging', which is miss-named anyway if this is 'SysErr' ... or this module should be named `Logging`

TODO: Ask the team - which is it?

Move the code for logged item function and type declaration into a file named `LoggedItem.js` in the `src/nmc` folder. (caps because it a class)

See `loggedItem.md`

add an import for the logged item into `SysErr.js`

```javascript
import LoggedItem from "./LoggedItem.js";
```

change `this.maxMessages = 500;` to `let maxMessages = 500;` and change the reference to it.

# @var

Change `var` to `let` and convert the annotation for field variables to jsdoc.

make maxMessages a const

```javascript
/** @var {Array} items - An array of cached LoggedItems. */
let _items = [];

/** @var {number} maxMessages - Maximum number of messages to cache before uploading. */
const maxMessages = 500;
```

change instances of `this.foo = function` to `function foo(`

add an export for the functions

```javascript
export { clear, getItems, logMessage, logError, uploadLog };
```
