# Initialization

Checkout the `nmc-plugin-platform` project

`Move` the `Platform[nnn].js` files to the plugin project root

# Plafrom Assets

Create a new file named `platformAssets.js`

Open a bash shell

```
grep "Images/Platform" ./PlatformComponent.js
```

Copy results to `platformAssets.js`

- Delete the comment
- Change "Images/Platform" to "import foo from "./images"
- Change `",` to `";` (comma to semi-colon)
- For each line change foo to the file name, without suffix or number prefix
- add a section

```javascript
export {};
```

- Copy the lines above
- Select them
- Change using selection and regex `from.*` to `,` (leading space infront of from)

Create a folder named `images` in the root and copy all referenced images from `nmc-6.1` into it.

## PlatformComponents.js

import the assests

```javascript
import * as images from "./platformAssets.js";
```

- Change `"Images/Platform/` to `images.`
- Change `.png"` to null
- Change `32x32_` to null
- Change `16x16_` to null

# verdaccio

# nmc-client

Delete the images from `NMCHTML/Platform/Images`

## webpack config

Add a file loader to `base.webpack.config`

```javascript
,
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[path][name].[ext]?[hash]",
            context: ""
          }
        }

```

## verdaccio

Checkout the `verdaccio` project and read the readme.

```
Terminal > Run task... > Start verdaccio
```

## Publish

Open a command prompt for the `nmc-plugin-platform` project and type

```
npm publish
```

## app.js

change `require("./platform/PlatformComponent.js");`

to `require("@nmc/nmc-plugin-platform/PlatformComponent.js");`

## NMCApp.js

change `import * as platformConstants from "../platform/platformConstants.js";`

to `import * as platformConstants from "@nmc/nmc-plugin-platform/platformConstants";`

```
npm install @nmc/nmc-plugin-platform
```
