# auto-run test and coverage

Open a terminal to the npm-client directory and start the test runner

```
npm run lcov ./test/nmc/PubSub-test.js --silent
```

This runs test and coverage for just the PubSub file.

It will re-run every time a file is changed because nodemon is used to start it. --watch cannot be used on the test because it will not relinquish control to the coverage tool (Istanbul) until the process is killed.

This is slower but runs coverage each time and is useful when creating tests for existing code.

# coverage

Open `nmc-client/coverage/PubSub.js.html` in the browser. Refresh each time a change is saved.

# debugging

Set a break in `PubSub-test.js` in the `1st parameter test` on the line that instantiates the object.

```javascript
let bus = new PubSub();
```

Set a break in `PubSub.js` on the line in the `publish` method invoking the internal (private) method.

```javascript
publish: function(message, data) {
  return publish(message, data, true, null);
```

Select `Debug single test (nmc-client)` the debug panel (ensure the PubSub-test.js file is selected/active)

Click the `Start debugging` icon/triangle

# some standard tests

- parameter is null
- parameter is undefined
- parameter is incorrect type
- parameter matches required value(s)
- returns expected type/value
- String parameters are empty
- parameter default is used (ES6).

When writing the tests we see several of the functions return different types. It is better to throw an exception if parameters are invalid, rather than return false. If the function returns true because it succeeded and false if not, then how does the invoking code know the parameter was valid but not found, for instance.

This is more evident in the unsubscribe method which could return undefined, false, true or a string value.

Returning the value doesn't make sense because the value could be true or false or 1 or 0

Returning true or false to indicate success makes sense but then errors should be thrown for invalid parameters - this is 'normal' behavior.
