Cut the creation and allocation of `nmc.[...]` namespace constants and paste into a file named `platformConstants.js` in the `plaform` folder.

Remove the instantiation code at the top

Add:

```javascript
/**
 * @module platformConstants
 */
```

Change the namespace constants to objects in the `platformConstants` module, e.g. change

```javascript
nmc.platform.menuTypes = nmc.platform.menuTypes;
```

to

```javascript
export let menuTypes = Object.freeze({
  Organizations: "nmc.platform.menuTypes.Organizations",
  [...]
});
```

i.e. Add `export let` to start, `= Object.freeze(` before `open brace` and `close parenthesis` after `close brace`

TODO: Discuss: OrganizationDeleted is defined twice. Last one wins but first one makes more sense. Second description states site deleted, so should it be SiteDeleted ?

Change the `/// <Summary` comments to

```javascript
/**
 * Description
 * @readonly
 * @enum
 * @property {string} Organizations
 */
```

TODO: Discuss @property with team. It's a duplicate of the entry - is it worth it.
Either delete the `/// <field` declarations or move them to the comment as @property

Note: Generate jsdoc to see an example

Add

```json
"jsdoc-file": "jsdoc -r --destination jsdoc -c jsdoc.conf"
```

to `package.json` scripts

Type `npm run jsdoc-file src/platform/platformConstants.js` in the console to generate the jsdoc and review.

Fix linting errors

TODO: Some are camel case some pascal case. Typically an object starts with a capital.
In an enum usually the enum is pascal case and the items are all caps.

It doesn't matter what is chosen, but be consistant!
Google recommends pascal case...
https://google.github.io/styleguide/javascriptguide.xml?showone=Naming#Naming
