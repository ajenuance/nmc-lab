# 404

The files referenced in `vendorCss.ejs` and `vendorScripts.ejs` must be added to the client in the NMCHTML folder.

Download the nmc project from http://git.nuancehce.com/stash/projects/NMS/NMC

## missing Stylesheets

The css files can be found in the nmc project under NMC/Content.

## missing scripts

The script files can be found in the nmc project under NMC/Scripts/lib.

There are also images and other files required.

Open the DEBUG CONSOLE tab and expand it. Highlight each entry for `404: file not found`, copy it from the 6.1 project into a similar folder structure in the 'new' project under a root folder named NMCHTML.

Clear the console using the icon in the top right and refresh the page to 'reduce the clutter'.

# Missing files

Eventually the errors should reduce to the following:

1. 404 (Not Found) [http://localhost:4014/Content/images/exclamation.png]
   This id due to code in `NMCApp.js`

   ```javascript
   var img = '<img src="../Content/images/exclamation.png"
   ```

   The image exists in NMC/Content/images so copy it to the project and change the code to:

   ```javascript
   var img = '<img src="Content/images/exclamation.png"
   ```

2. 404 (Not Found) [http://localhost:4014/NMCHTML/Content/Images/arrowUP.png]
   This image does not exist in the nmc project

   A search finds the reference in main.css for `../Images/arrowUP.png`

   TODO: Ask the team what the intent is.

3. 404 (Not Found) [http://localhost:4014/Scripts/main.js]
   This is due to the line in `appScripts.js` for require

   ```javascript
   <script
     data-main="../Scripts/main.js"
     src="Scripts/lib/require.js"
     type="text/javascript"
   />
   ```

   The line can be deleted, since the file is also included on the next line and require will no longer be needed in this project.

   TODO: Ask the team why this is coded

4. 404 (Not Found) [http://localhost:4014/NMCHTML/@ViewData[%22nmcVirtualDir%22]/Home/PreLogout]
   This is coded in `bodyScripts.ejs` as a parameter in the invocation of `NMCApp.Init`.

   This is part of the code to cope with relative addressing.

   Change it to an empty string `""` for now. This will be addressed later.

5. Ignore the errors for `locales` for now
