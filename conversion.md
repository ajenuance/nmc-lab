# Coding conventions.

See `conventions.md`

# login and supporting pages

This section TBD

# Checkout

Checkout the BaseProject branch

# Home page

See the homePage.md file in the readme folder of this project.

# Initial commit

<span style="color:magenta">commit the project (StepOne)</span>

# File server

The files referenced in vendorCss and vendorScripts must be added to the file server.

see `fileServer.md` in the readme folder of this project.

# Remaining errors in home page

At this point the page builds but with some @ symbols and the Project Template message.

## @if

There is an @if coded in `appHeader.ejs` which appears to only include an element to request help if the locale is not "FR".

TODO: Ask the team about this. For now, remove the conditional.

## @\*

There is an @\* \*@ block coded in `documentTabNav.ejs`. This is a cshtml comment and can be removed.

# Commit 404 resolution

<span style="color:magenta">commit the project (StepTwo)</span>

# Context menu

Code at the very bottom of NMCApp disables the context menu for anything but input, textarea and label.

TODO: Discuss why ?

Inserting the following code in the function can override that with the ctrl key, which is helpful for debugging.

```javascript
if (e.ctrlKey) {
  return true;
}
```

# Converting body.onXXX

`app.js` is the constructor for the application. There is code included inline which can be transferred to the constructor.

See `app.md`.

<span style="color:magenta">commit the project (StepThree)</span>

# Session info

There is some initialization code in `bodyScripts.js` that needs to be moved to the application constructor in `app.js.`

However, that code invokes methods on `NMCApp` to initialize it and also some session info used in the cshtml which is no longer available.

In order to obtain the information required to initialize the project we need an api call to the server.

see `sessionInfo.md`

# Debugging.

Now is a good time to confirm the debugging works.

see `debugging.md`

# axios

See `axios.md`

<span style="color:magenta">commit the project (StepFour)</span>

# Dialogs

As part of the changes for `SysErr.js` a new file was created named `dialogs.js`

See `dialogs.md`

<span style="color:magenta">commit the project (StepFive)</span>

# Corrections

- ConfirmDialog was missing
- `virtualDirectoryName_Ex` misspelt in `nmcUtils.js`

# Convert NMCApp.js to a module

See `NMCApp.md`

# Initializing the application

Now we can move the initialization code in `appScripts.js` and `bodyScripts.js`

See `init.md`

<span style="color:magenta">commit the project (StepSix)</span>

# 404 logout

See `logout.md`

# Update dependencies

The project has been defined for a while. An audit should be taken periodically to keep the project up to date.

see `audit.md`

<span style="color:magenta">commit the project (StepSeven)</span>

# nameSpaceExists.

Why is this eval? ...and the return type is boolean.

```javascript
/**
 * @description Check if the given namespace exists.
 *
 * @param {string} nameSpace - The namespace to be found.
 *
 * @returns {boolean} True if the namespace exists.
 */
function nameSpaceExists(nameSpace) {
  return window[nameSpace] ? true : false;
}
```

# import and export warnings.

Resolve warnings reported in the build terminal

# fix debug for unit tests

<span style="color:magenta">commit the project (StepEight)</span>

# Convert Platform

See `platformComponent.md`

# NMCApp revisited

NMCApp uses `nmc.platform.[name]`. This must be revisited.

<span style="color:magenta">commit the project (StepNine)</span>

# Linting PlatformComponent.

See `PlatformComponentLint.md`

<span style="color:magenta">commit the project (StepTen)</span>

# Creating Platform as a deployable package.

See `nmc-plugin-platform.md`

<span style="color:magenta">commit the project (StepEleven)</span>

# Abstracting namespace

See `namespace.md`

# PubSub

See `PubSub.md`

<span style="color:magenta">commit the project (StepTwelve)</span>

# JSDoc

```
npm run jsdoc
```

Open `nmc-client/jsdoc/index.html`

See that almost everything is a global.

Add `@module` to the doc comment for `PubSub.js`

re-run jsdoc

Note: This is only required because `PubSub.js` has not been converted to an ES6 module or a class.

see `circularRef.md`

# PlatformStrings

i18n was added to the platform component but neither the strings nor i18n configuration were implemented.

see `platformLocales.md`

<span style="color:magenta">commit the project (StepThirteen)</span>

# corrections

Change `platform:` to `framework:` in `NMCApp.js`

We still have `@(session...` in the header

see `appHeader.md`

see `platformComponentInit.md`

<span style="color:magenta">commit the project (StepFourteen)</span>

.
.
.

# Lastly

List the TODOs

# Replace jQuery:

- `$(...).each` === ES6 Array (maps, filters, sort, etc.)
- `$(...)` === DOM querySelector
- `$.isFunction` === `typeof x === "function"`
