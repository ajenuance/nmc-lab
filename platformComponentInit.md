# PlatformComponents

Do the following one section at a time while the dev environment is running. That way any issue has a chance to manifest more immediately
Move functions after all the inline code. This is good practice. Furthermore, if there is a lot of inline code it is better to encapsulate it into one or more functions with similar code. This makes it easier to read, easier to refactor and easier to test.

Find each section of inline code and encapsulate in a function, starting `function init_1()` and increment. We can rename later.

Move variable declarations to the top if used as global else encapsulate in the function.

Invoke each one at the top of the code.

var declarations should be refactored to use let.

Now review each init function for embedded variables.

Now we have abstracted the code it must be executed `after` the language files have been loaded. This causes a problem because the locale files are retrieved asynchronously, which means control is passed back to `app.js` and `NMCApp::initUi` will be invoked, which runs some jQuery code to 'convert' items to jQuery UI objects. If the PlatformCompnent has not built yet because it is waiting for the locale to finish then the elements it adds will not be found by the jQuery invocation(s).

This can be avoided using Promises. Add the following code to `PlatformComponent.js`.

```javascript
/**
 * @description Invoked to notify the plugin it can establish itself in the NMC UI.
 * The function returns a promise since it makes an asynchronous call to load the i18n namespace.
 * If that invocationfails then the reject method of the Promise is invoked, otherwise the UI is initialized and resolve is invoked.
 *
 * @returns {Promise} A promise.
 */
export function start() {
  return new Promise((resolve, reject) => {
    i18n.loadNamespaces("platform", err => {
      if (err) {
        reject(err);
      } else {
        try {
          initializeComponents();
          resolve();
        } catch (e) {
          reject(e);
        }
      }
    });
  });
}

/**
 * @description Invoked once the i18n files have loaded.
 *
 * @returns {undefined} Nothing.
 */
function initializeComponents() {
  init_1();
  [...]
```

Commit the changes to git, otherwise the publish will fail.

Build the ProjectComponent` project and publish.

```
npm run build
npm run pub.patch
```

Update the plugin in the `nmc-client` project

```
npm update @nmc/nm-plugin-platform
```
