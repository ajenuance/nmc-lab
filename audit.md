# npm update

As time progresses new versions of the dependency libraries are released with new features and bug fixes.

It is prudent to periodically check the project.

```
npm outdated
```

To update minor releases

```
npm update
```

To update to the latest version

```
npm install --save-dev package@* [package@*]
```

To see a list of all packages or a set of packages

```
npm list [package]
``
```

# nmc-client

The client project is several releases behind babel. The latest babel project has been updated to the 'new' package syntax `@babel/[module]`

To use this all previous babel packages must be uninstalled. There are many of these. You can type npm uninstall package [,package] or delete the node_modules folder and lock file...and then hand edit-the `package.json` file.

Note also that the istanbul package being used is outdated. The istanbul project now provides the `nyc` plugin.

Finally, the 'new' babel requires different loaders be used for testing in the `package.json` scripts.

# .babelrc

The `.babelrc` configuration file must be updated to reflect these changes.

```javascript
{
  "presets": [
    [
      "@babel/preset-env",
      {
        "targets": {
          "browsers": ["last 2 versions", "ie >= 10"]
        }
      }
    ]
  ],
  "env": {
    "test": {
      "plugins": ["istanbul"]
    }
  },
  "plugins": [
    ["@babel/plugin-proposal-decorators", { "decoratorsBeforeExport": false }],
    "@babel/plugin-syntax-dynamic-import"
  ]
}
```

# .gitignore

Add the output from nyc to the ignore list

```javascript
coverage.nyc_output;
```

# .nycrc

Add a configuration file named `.nycrc` for the `nyc` package.

```javascript
{
  "require": [
    "@babel/register"
  ],
  "include": [
    "src/**/*.js"
  ],
  "exclude": [
    "!**/node_modules/**",
    "node_modules"
  ],
  "reporter": [
    "html",
    "text",
    "text-summary"
  ],
  "sourceMap": false,
  "instrument": false,
  "all": true
}
```

# launch.json

The change for `@babel/register` must also be applied to the `launch.json` file in the `.vscode` folder.

# Package.json scripts

```json
    "test": "cross-env NODE_ENV=test mocha --recursive --require @babel/register --require ignore-styles --watch --",
    "test-once": "cross-env NODE_ENV=test mocha --recursive --require @babel/register --require ignore-styles --",
    "coverage": "cross-env NODE_ENV=test nyc --all mocha --recursive",
```

# devDependencies

```
npm uninstall istanbul babel-core babel-eslint babel-loader babel-plugin-syntax-dynamic-import babel-plugin-transform-decorators-legacy babel-polyfill babel-preset-env babel-preset-stage-3 babel-register
```

```
npm install --save-dev nyc @babel/core @babel/plugin-proposal-decorators @babel/plugin-syntax-dynamic-import @babel/polyfill @babel/preset-env @babel/register babel-eslint babel-loader
```

Run an `audit` after. It may be necessary to run `npm install`

## re-install

It is recommended to delete the `package-lock` file, the `node_modules` folder and `npm install`.

# export/import

The files must be updated to use the ES6 `import` and `export` structure.

## app.js

```javascript
import * as main from "./main.js";
import * as SysErr from "./nmc/SysErr.js";
import * as NMCApp from "./nmc/NMCApp.js";
```

## NMCApp.js

change `module.exports = {` to `export {` and reverse the allocations from `name:publicName` to `publicName as name`

## module.exports

Change `main.js`, `SysErr.js`, `dialogs.js`, `nmcUtils.js` from `module.exports = {` to `export {`
