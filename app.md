# app.js

The body tag in `index.ejs` has some handlers added. These should be moved to the app constructor.

The assignment to variables can be removed since they are also assigned in `main.js`

```javascript
onload = "showWarningMsgOnBeforeunload = logoutAfterUnload = true;";
```

The methods referenced in the body tag are defined in `main.js`

Change the body tag in `index.ejs` to `<body>` and replace the code in the `app.js` constructor with the following:

```javascript
main.checkBrowserInfo();
window.onbeforeunload = () => {
  return main.onBeforeUnloadNmcMainHandler;
};
window.onunload = main.onUnloadNmcMainHandler;
```

`main.js` is included as a script tag in `appScripts.ejs`.

1. Remove the tag

2. Add an import to `app.js`

   ```javascript
   import main from "./main.js";
   ```

3. Move `main.js` to the src folder

# Modularize `main.js`

see `main.md`

# body tag

Several items were removed from the body tag which must be invoked in app.js:

Add the following into the constructor of `app.js`

```javascript
main.checkBrowserInfo();
window.onbeforeunload = () => {
  return main.onBeforeUnloadNmcMainHandler;
};
window.onunload = main.onUnloadNmcMainHandler;
```
