# Ajax

Consider using axios promises instead of jquery ajax

# undefined

There are several places where `if(typeof foo !== "undefined")` is coded and then foo is accessed.

This can fail if the variable is null.

If the intention of the code is to ask if the item can be accesssed then it should be `if(foo)`

Furthermore, if the code then tries to invoke a method it aught to be...

```javascript
if (foo && foo.method) {
  foo.method();
}
```

# naming

There are several cases where the function names do not seem to describe what the function does.

For instance, `PlatformComponents::searchClientVersion` makes trendsMenu active or adds a clientVersion tab. See also `PlatformComponents::searchOrganizations` which makes the `search organizations` tab active

Several functions are also named using pascal case when they should be camel case, e.g. `PlatformComponents::ShowEditOrganizationTokens`

# Session Info

NMCApp.getNMCSessionInfo() may want to create a separate object for this rather than needing to include NMCApp every where?

Perhaps also NMCApp.getMaxSearchNumber() should be in sessionInfo?

Forgery token needs to be in session info

TODO: NMCApp.js::publicLoadWithPost added after conversion
