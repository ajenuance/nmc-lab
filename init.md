Now we can move the initialization code in `appScripts.js` and `bodyScripts.js` into `app.js`

# import into project

Add an import into `app.js`

```javascript
import * as NMCApp from "./nmc/NMCApp.js";
```

Review DEBUG CONSOLE

# appScripts.ejs

Remove script tags for `NMCApp` and `sysErr`

Remove code to write the "logged in" message

# bodyScripts.ejs

Remove the comment? TODO: Discuss with team.

Remove the script at the top invoking System.diagnostic...

Remove script tag for `NMCFrameworkStrings.js`

## app.js

Move the code from the first script section into `app.js` after the `SysErr.logmessage` statement

Delete the assignment to session

Create a new method (after `publicInit`) in `NMCApp.js` named `publicInitUi` and move the remaining code from `bodyScripts.ejs` into it.

Change the references to `NMCApp` in the newly added code.

Add an entry in the `export` section.

# axios promise

Add a `then` clause to the promise returned by the ajax call in`app.js`

```javascript
.then(() => {
  NMCApp.initUi();
});
```

Now the include for `bodyScripts.ejs` can be removed from `index.ejs` and the file can deleted.

Do the same for `appScripts.ejs`

# DEBUG CONSOLE

Now address the remaining errors in the console.

## checkBrowerInfo

Correct the typo in `main.js` from `checkBrowerInfo` to `checkBrowserInfo`

## arrowUP.png

This is in `main.css`. Notice the declaration below uses `arrowDown.png`

For now, copy the file named arrow-up.png from the 6.1 folder into the project.

TODO: Resolve this with the team.

# RequestVerificationToken

If we let the timer count down eventually we see an error regarding `__RequestVerificationToken`

This is because the HTML is no longer built server side and so this input element does not exist.

This should be added to the session object returned by the ajax invocation.

# Session expiration

The session expiration dialog is rendered immediately

Set a break on the invocation of `NMCApp.init` in `app.js` and refresh the page.

We see the session info is in `data`.

Press F5 or click the continue icon in the debug bar.

Change the call to `startSession` to pass `json.data`

browser-sync should refresh the page and the breakpoint should be hit.

See now that `NMCInactivityTimeOut` is missing. Add it to `users.json` in the `data-server` project.

```json
"NMCInactivityTimeOut":1,
```
