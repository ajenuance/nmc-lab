LoggedItem is a function so convert to a class by surrounding it in:

```javascript
export default class LoggedItem {
  [code goes here]
}
```

change the `LoggedItem` function to be the constructor

Change the `var` declarations to `this.`

Change all the `this.foo = function` to `foo()` and add the jsdoc

Change `toPlainObject` to return the object using `foo:this.foo`

#MessageType

Rename the `Logging.MessageType` to match `LoggedItem` to match the class.

Use `Object.freeze`

Use jsdoc `@property` and `@var` to document the items

```javascript
/**
 * @property {object} MessageType - An enumeration of the message types used by a LoggedItem.
 */
LoggedItem.MessageType = Object.freeze({
  /** @var {number} Info - Informational message. */
  Info: 0,
  /** @var {number} Error - Error message */
  Error: 1
});
```
