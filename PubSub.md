# PubSub

Abstract `PubSub` from `NMCApp.js`

Just put `export default` in front of the function declaration.

Add `import` in `NMCApp.js` and change invocations of `publicPubSub` to `PubSub`

Remove the export

# Unit test

Copy the `PubSub-test.js` from the `StepTwelve` folder of the `nmc-lab` project into `test/nmc/`

## running the test

There is a custom reporter in the StepTwelve folder of the `nmc-lab` project. Copy it to the test folder

add the following scripts to `package.json`

```json
{
  "test-min": "cross-env NODE_ENV=test mocha --reporter ./test/mocha-reporter-min.js --recursive --require @babel/register --require ignore-styles --watch --"
}
```

To run the test each time a change is made and use a reporter with minimal output, use...

```
npm run test-min ./test/nmc/PubSub-test.js
```

## Coverage

To also run coverage use...

add the following scripts to `package.json`

```json
{
  "lcov": "cross-env NODE_ENV=test nodemon --exec nyc --all npm run lcov-test",
  "lcov-test": "cross-env NODE_ENV=test mocha --reporter ./test/mocha-reporter-min.js --recursive --require @babel/register --require ignore-styles --"
}
```

```
npm run lcov ./test/nmc/PubSub-test.js
```

### npm ERR

Coverage will result in several `npm ERR!` messages when one or more tests fail.

```
npm ERR! code ELIFECYCLE
npm ERR! errno 8
```

where `errno n` is the number of tests that failed.

This can be suppressed by adding --silent to the end of the npm command

```
npm run lcov ./test/nmc/PubSub-test.js --silent
```

### file subset

To limit the coverage output to the `PubSub` file change the include pattern in the .nycrc file.

DON'T FORGET TO CHANGE IT BACK!

# write the tests

see `PubSub-test.md`.

# Platform component

Notice that PlatformComponent does not fail, even though it invokes `new NMCApp.PubSub()`

This is because it will fail at runtime and is not invoked by the initialization code.

Update `PlatformComponent.js` to use the `PubSub` module.

Update the patch version and publish.

Open a terminal to the `nmc-plugin-platform` project.

```
npm version patch
npm publish
```

Open a terminal to the `nmc-client` project

```
npm update @nmc/nmc-plugin-platform
```

You will see the version in `package.json` has changed.

# peerDependency

TODO:

The `package.json` file in the `nmc-plugin-platform` project should really include NMCApp package as a `peerDependency` ... put NMCApp is not a package...yet.
