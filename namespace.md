# namespace

`NMCApp.js` contains code to create a namespace.

Create a new file `src/nmc/namespace.js`

Copy the two methods and merge

- No need for a function with a single line if it is only executed once.
- No need to eval when `window["value"]` does the same.

```javascript
/**
 * @description Creates a namespace.
 *
 * @param {string} nameSpace - The namespace required.
 *
 * @returns {undefined} Nothing.
 */
export default function createNameSpace(nameSpace) {
  let chk = false;
  let cob = "";
  let spc = nameSpace.split(".");
  for (let i = 0; i < spc.length; i++) {
    if (cob !== "") {
      cob += ".";
    }
    cob += spc[i];
    chk = window[cob] ? true : false;
    if (!chk) {
      window[cob] = {};
    }
  }
  if (chk) {
    throw "Namespace: " + nameSpace + " is already defined.";
  }
}
```

# Testing

Create a file `test/nmc/namespace-test.js`

Add tests for invoking createNameSpace with

- null
- objects that are not string
- one level namespace
- two level namespace
- existing namespace

Hint 1:

```javascript
import chai, { expect } from "chai";
import { JSDOM } from "jsdom";
import createNameSpace from "../../src/nmc/namespace.js";

var dom = new JSDOM("<!DOCTYPE html>");
global.document = dom.window.document;
global.window = dom.window;
```

Hint 2:

```javascript
expect(() => createNameSpace()).to.throw(TypeError, "Invalid parameter type");
```

Run the tests

```
npm run test ./test/nmc/namespace-test.js
```

TODO: Discuss: There is no validation of namespace content. It _might_ be best to limit to ascii characters [A-Za-z0-9\.]

Delete the functions and `export` from `NMCApp.js`.

This is probably academic as we move to modules, but a good TDD excercise.
