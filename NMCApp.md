# modularize

Move `NMCHTML/NMCApp.js` into `src/nmc`

Remove the top (3-5) lines that make it self executable

Remove the self execution at the bottom of the file

Change the return into `export {}`

# eqeqeq

TEMPORARILY disable the eqeqeq rule by adding a comment at the top of the file.

```javascript
/* eslint no-use-before-define: 0 */
```

TODO: Each instance must be examined for cooercion so this is done to temporarily 'hide' the errors.

This must be removed once all other linting errors have been addressed.

# functions

Change `var foo = function` to `function foo(`

Delete `publicUuid` since it has been converted into a separate module.

TODO: publicEventTypes is defined in the middle of functions. Should be moved to a separate module.

TODO: Util section should be moved to separate module(s)

# linting

Fix linting/jsdoc errors

TODO: Ask why `nameSpaceExists` is eval instead of `window[nameSpace]`. For now, add it in .`eslintrc.js` as a global (after `env` before `extends`)

```json
  globals: {
    NE: true
  },
```

change var to let while at top

sessionTimeoutDialog has been abstracted. Declare it as a constant at the top

```javascript
const sessionTimeoutDialog = new SessionTimeoutDialog();
```

Add imports

```javascript
import SessionTimeoutDialog from "./dialogs/SessionTimeoutDialog.js";
import dialogs from "./dialogs.js";
```

```javascript
// eslint-disable-next-line no-unused-vars
let ribbonControlId = "";
```

TODO: Requires is not defined - presumably to load libraries since it create a script. This can be deleted because it will probably need to be replaced with lazy loaded ES6 requires.

`export` must be outside the braces

Close publicInit and remove closing brace from end of file. This is because the functions declared in the publicInit function will now be created as part of the module.

publicShowNMCConfirmWindow should have been done in StepFive

# NMCApp.eventTypes

Create `nmcConstants.js` in the `nmc` folder

Cut the definitions from NMCApp.js and paste into the file.

```javascript
let event = Object.freeze({
  OnHide: "NMCApp.eventTypes.OnHide",
  OnShow: "NMCApp.eventTypes.OnShow",
  OnClick: "NMCApp.eventTypes.OnClick",
  OnBeforeClose: "NMCApp.eventTypes.OnBeforeClose",
  HideOrShowRibbonSection: "NMCApp.eventTypes.HideOrShowRibbonSection"
});

export { event };
```

Add an import

```javascript
import { event as NmcEvent } from "./NMCConstants.js";
```

delete the definitions from `NMCApp.js`

# NMCApp.fireEvent

Invocations can be renamed to `publicFireEvent`

# NMCApp.RibbonButton

Access to `NMCApp.RibbonButton` can be renamed to `publicRibbonButton`

# publicAddDocumentTab circular dependency

publicAddDocumentTab contains references to platform tabTypes and sting values.

This is potentially not good. The NMCApp is the scaffolding for the application.

Platform adds to the scaffold and has a dependency on NMCApp, so now we have a circular dependency.

TODO: Discuss with the team.

Add `nmc` to the `global` section of `.eslintrc.js`

# publicDocumentTabChanged

Expects an Array for the parameters but does nothing if there is more or less than 1 item.

TODO: Discuss why an array is passed if only one item is required?

# NMCApp.showNMCConfirmWindow

references to `NMCApp.showNMCConfirmWindow` can be changed to `dialogs.showNMCConfirmWindow`

# NMCApp.DocumentTab

This should eventually be an object, for now change it to `publicDocumentTab`

# publicPubsub::hasKeys

TODO: Discuss with the team. If the object has at least one property of its own. false if all the properties are inherited. What is this for?

# var to let

Find all `var`s and change to `let`, where possible.

# eqeqeq

Remove the comment at the top of the file and review each error.
