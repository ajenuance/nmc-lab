1. Kill the data-server processes.
   Type `Ctrl+C` several times.

2. Set a breakpoint in `SessionInfo.js` of the `nmc-data-server` project on the line

   ```javascript
   let name = getUser(req);
   ```

   Note how the breakpoint is hollow rather then solid red, as they were in the previous step (2)

   This is because there is already a deubg process running for the browser client. This can be seen in the CALL STACK section of the debug panel (on the left of VSCode)

3. Restart the process using `Debug data-server (nmc-data-server)` from the debug drop-list.
   Notice how the breakpoints are now solid red and a second entry has been added to the CALLSTACK section.

   Click the Chrome item in the CALL STACK and they bceom hollow.

   Click the Node item in the CALL STACK and they are solid red.

4. Open a browser and enter http://localhost:4013/NMCHTML/SessionInfo, or hold `Ctrl` and click this link.

   The breakpoint is now hit, but when the statement is stepped over we see it is null, this is because there is no login provided by the data-server.

   Press F5 to continue, or click the continue button in the debug bar.

   This results in `Unauthorized` because there is no authentication header.

   1. Change the port to 4010 and use admin:admin for the credentials.

   2. The breakpoint in `SessionInfo.js` will now be activated.
      Review the various sections in the debug panel.

      Note that command can also be entered into the command line of the DEBUG CONSOLE panel.

   3. Press F5 or click continue in the debug bar.
      Results are now shown in the browser.

5. Kill all processes and restart them using a task so they are no longer in debug mode.
