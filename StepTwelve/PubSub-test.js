import { expect } from "chai";
import PubSub from "../../src/nmc/PubSub";

describe("PubSub", function() {
  it("Instantiation", () => {
    expect(() => new PubSub()).not.to.throw();
  });

  checkPublish();
  checkASyncPublish();
  checkMessageHasSubscribers();
  checkSubscribe();
  checkUnsubscribe();

  describe("clearAllSubscriptions", function() {
    it("should not throw exceptions", () => {
      let bus = new PubSub();
      expect(bus.clearAllSubscriptions()).to.not.throw;
    });
  });
});

function checkPublish() {}

function checkASyncPublish() {}

function checkMessageHasSubscribers() {}

function checkSubscribe() {}

function checkUnsubscribe() {}
