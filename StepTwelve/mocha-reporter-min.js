/* eslint no-console:0 */

/* Ref: https://github.com/mochajs/mocha/blob/master/lib/reporters/base.js */

var mocha = require("mocha");
module.exports = MyReporter;

/**
 * @description Custom reporter for mocha tests.
 *
 * @param {Object} runner - The test runner.
 *
 * @returns {undefined} Nothing.
 */
function MyReporter(runner) {
  mocha.reporters.Base.call(this, runner);
  var passes = 0;
  var failures = 0;

  runner.on("start", function() {
    // clear screen
    process.stdout.write("\u001b[2J");
    // set cursor position
    // process.stdout.write("\u001b[1;3H");
  });

  runner.on("pass", function(test) {
    passes++;
    /*
    console.log(
      mocha.reporters.Base.color(
        "bright pass",
        mocha.reporters.Base.symbols.ok
      ) + " %s",
      test.fullTitle()
    );
    */
  });

  runner.on("fail", function(test, err) {
    failures++;
    console.log(
      mocha.reporters.Base.color(
        "bright fail",
        mocha.reporters.Base.symbols.err
      ) + " %s; %s",
      test.fullTitle(),
      err.message
    );
  });

  runner.on("end", function() {
    console.log(mocha.reporters.Base.color("pass", "pass"), passes);
    console.log(mocha.reporters.Base.color("fail", "fail"), failures);
  });
}
