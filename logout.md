# PreLogout

Let the timer run down and we see a 404 for `/Home/PreLogout`. This is invoked before logging out to remove the NMS session.

On success the application will be routed to `/Home/Logout`

Neither of these server side processes exist, so create them.

- Add an entry in `server/routes.js`
- Create a `Home` folder in `server/routes/NMCHTML`
- Create the files in the Home folder

```javascript
let express = require("express");
let router = express.Router();

router.get("/", function(req, res) {
  res.json("Logged out");
});

module.exports = router;
```

PreLogout is a post and should return en empty string.

```javascript
router.post("/", function(req, res) {
  res.json("");
});
```

Change the `NMCInactivityTimeOut` in `users.json` to a reasonable number, i.e. 5.

Now we have a 404 GET for /NMCHTML/Home/KeepSessionAlive

Add that and return `OK`

Restart the server.

Confirm we now see a 200 for KeepSeesionAlive
