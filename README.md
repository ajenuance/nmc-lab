<h1 style="color:cyan">Follow the instructions in this readme and then open conversion.md</h1>

# Extensions

Required extensions for VS Code:

2. ESLint - Dirk Baeumer
3. Prettier - Esben Petersen

Recommended extension:

1. StatusBar Debugger - Fabio Spampinato
2. GitLens - Eric Amodio
3. TODO Highlighter - Wayou Liu

Required extension to debug Chrome in VS Code:

1. Debugger for Chrome - Microsoft
   Without this extension debugging can be performed in Chrome Dev Tools

# Prerequisites

Checkout the `nmc-proxy`, `nmc-data-server` and `nmc-client` projects.

Open a terminal to each one and type

```
npm install
```

# Setup

1. Terminal > Run Task... > Start app server (nmc-client)
   If it fails with

   ```
   ENOTEMPTY: directory not empty,
   ```

   then close the terminal and try again.

2. Open a browser and enter `http://localhost:4014/NMCHTML/` into the address bar.

# Orphaned tasks

Occassionally a task will remain running once all terminals have been closed.

These can be killed using...

```
Win: Taskkill /PID [process-id] /F
OSx: kill [process-id]
```

Use the `Attach by Process ID` launch configuration from the debug drop list to see a list of running tasks.

To avoid orphaned tasks...

```
Always use ctlr+c to cancel processes running in a terminal. In some cases it is neccessary to pres ctrl+c multiple times.
```
