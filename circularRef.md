# Typo in step 12

`platform.:` - remove the dot

# Issues

NMCApp uses `i18n.t("platform:`. NMCApp is the scaffolding. Platform should depend on it but it should not depend on platform. That is a circular dependency.

There are three repreated instances of `showConfirmationDialog` using ...

```javascript
  i18n.t("platform:UnsavedInformationCaption"),
  i18n.t("platform:UnsavedInformationMsg"),
  i18n.t("platform:Yes"),
  i18n.t("platform:Cancel"),
```

`Yes` and `Cancel` are also defined in `framework.json`, so those should be used.

The other strings are defined as

```javascript
nmc.platform.strings.UnsavedInformationCaption = "Unsaved Information";
nmc.platform.strings.UnsavedInformationMsg =
  "There is information that has not been saved.  Do you want to lose your changes and log out?";

nmc.platform.strings.UnsavedInformationCaption =
  "Informations non enregistrées";
nmc.platform.strings.UnsavedInformationMsg =
  "Certaines informations n'ont pas été enregistrées. Si vous vous déconnectez, les informations seront perdues. Voulez-vous vraiment vous déconnecter?";
```

If we add those to framework it will remove the dependency on Platform

Update the code to use framework constants.

```javascript
i18n.t("framework:UnsavedInformationCaption"),
i18n.t("framework:UnsavedInformationMsg"),
i18n.t("framework:Yes"),
i18n.t("framework:Cancel"),
```
