The file is defined as...

```html
<div id="welcomeNuance"><label class="NMCLabel">Welcome...</label></div>
```

First, this is not internationalized and second it needs the values inserted.

Remove the content in the label.

Remember, `app.js` was changed to retrieve the session information and invoke `NMCApp.setNMCSessionInfo`

This is invoked with multiple items when it would make more sense to send the json object, so change the invocation in `app.js` to...

```javascript
NMCApp.setNMCSessionInfo(session);
```

Change `NMCApp.js` to then use that object.

```javascript
function publicSetNMCSessionInfo(session) {
```

Before the sessionInfo assignment add code to set the header.

Note. We use the 'new' `querySelector` method.

```javascript
document.querySelector("#welcomeNuance .NMCLabel").innerHTML = `${i18n.t(
  "framework:welcome"
)} ${session.lastName}, ${session.firstName}`;
```

Add an entry in the locale files before `"Yes":`

```json
  "welcome": "Welcome",
```

Note: Compare the two locale files - there are entries missing in each. (Yes, NuanceManagementConsole)

TODO: Explore using destructuring to assign the sessionInfo

As an aside, there are many instances of `/****` comments in NMCApp.js. Rename them to `/* *` so they are not treated as JSDoc.

# Confirm i18n

To confirm the locales are working change the fallback value in `src/i18n/i18nOptions`

The framework values change but platform does not.
