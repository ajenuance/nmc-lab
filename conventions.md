The following are some suggested coding conventions which can help make code more readable and testable but are not (currently) enforcable via eslint or prettier.

# jsdoc

Separate the description, param and return items with an empty line.

```javascript
/**
 * @description The description.
 *
 * @param {string} one - the first one
 * @param {string} two - Another one.
 *
 * @returns {undefined} Nothing.
 */
```

Always start a function declaration after an empty line, unless its the first item in the file.

# naming conventions

## files

Files defining classes should be named using pascal case, all others should be camel case.

## functions

Functions that declare objects should be named using pascal case, others should be camel case.

function names should indicate what they do, usually `[verb][object]`

Avoid using underscores at the start of the name. This is convention for a system function and can confuse or possibly clash. It has no meaning in modern javascript modules and classes.

### examples

get and set are obvious, e.g. getOrganization or getOrganizationName

`showOrganizationTab` v's `searchOrganizations`. searchOrganizations would imply that...
`[object]`Organizations will be `[verb]`searched

ref: platformComponents.js

# imports

import statements should be at the top of the file.

3rd party packages should be declared first, followed by project packages.

# Scoping

Module/file scoped variables should be defined at the top of the file, after imports.

# export

It is preferable to define...

```javascript
export function function-name() {
```

...rather than...

```javascript
export {
  function-name,
  function-name
}
```

Exported functions should be defined after all local functions, i.e. at the end of the file.

# Inline and anonymous function

Avoid whenever possible. Encapsulate code in separate function declaration and use a reference. This will make it more easily testable.

# modularization

Keep the files small. ~500 lines if possible. >1000 is probably a candidate for refactoring.

This makes the code more managable and more testable.

# Empty lines

Dont be afraid to use them to delineate blocks of code. It makes the code more readable. They will be removed during minifaction.

# variable declaration

Declare each on a new line.

Don't use comma separated variable declarations. It is less readable and unneccessary, saving nothing.

# Consistencey

Be consistent in the naming.

Compile a set of approved prefixes.

Some are obvious, such as get/set/sort

when similar verbs can be used, pick one and reject the other to avoid ambiguity.

Examples:

```javascript
Approved

find : used when searching a set of items and returning ones that match the criteria
fetch : used to retrieve data from the server
read : used to read a file or local storage
write : inverse of read
create: create and return an item
show : is the verb prefix for all methods that cause various ui elements to be visible
activate : verb prefix for methods that make a visible UI element 'active'

avoid

make : use create
reveal : use show
on[event] : eg onClick, it is hard to infer what the code does.
```
