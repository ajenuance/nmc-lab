# Separation of concern

There are several dialogs created in `NMCApp.js` which should be abstracted into a separate module. This will facilitate testing in isolation and remove the need to import `NMCApp.js` everywhere.

1. Create a folder named `dialogs` in `src/nmc`
2. Create a new file named `ConfirmationDialog.js` in the dialogs folder.
3. Cut the code from `NMCApp.js` which creates the confirmation dialog.
   There are two comments indicating the start of the code, which ends before code invoking `$(window).resize`.

   At time of writing this...the lines in v1.6 were at 270 - 329

# ConfirmationDialog class

This is going to be a class, so surround the code in:

```javascript
/**
 * @typedef {Object} ConfirmationDialog
 *
 * @returns {ConfirmationDialog} An instance of ErrorDialog.
 */
export default class ConfirmationDialog {
  /**
   * @description Constructs a new instance of ConfirmationDialog.
   *
   * @returns {ConfirmationDialog} An instance of ConfirmationDialog.
   */
  constructor() {
    [code goes here]
  }
}
```

# Internationalization

The dialog will need i18n, so add an import:

```javascript
import i18next from "i18next";
```

# uuid

The code also invokes a method of `NMCApp.js` named `uuid`

This is used everywhere and should be abtracted into it's own module.

At time of writing that was lines 102 - 135 in `NMCApp.js`. Cut and paste them into a file named `uuid.js` in `src/nmc`

TODO: Explore using the uuid package in npm.

# NMCApp.uuid

import uuid into the dialog class and change all invocations of `NMCApp.uuid` to use the `uuid.js` module.

```javascript
import uuid from "../uuid.js";
```

# Code conversion

1. There are (four) assignments at the top of the constructor which should be converted to `let` statements.

   These variables are declared in `NMCApp.js` and should be removed.

2. Change the `var` assignment to `let` and use a template string.
   Put each attribute on a new line, formatting the HTML so it is obvious.

   Change the code to use substiution, e.g. \${var_name}

3. Change to use `i18n`
   There are declarations in `frameworkString.js` for `YES` and `CANCEL` which should be copied into the locale files.

   Add two assignments before the template string to assign `yes` and `cancel` then use those in the template string.

   Note: Add them in alphabetical order since it will become much easier to manage once there are many declarations.

4. Move the logic for `publicShowNMCConfirmationWindow` into the dialog

5. The code in the dialog will need to access the various elements, so add:

```javascript
this.dialog = $("#" + dialogId);
this.yesButton = $("#" + yesButtonId);
this.noButton = $("#" + noButtonId);
this.confirmationMessage = $("#" + confirmationMessageId);
```

..after the declaration of the template string, and change the dialog from

```javascript
$("#" + confirmationDialogId).dialog({
```

to

```javascript
this.dialog.dialog({
```

6. Delete the line adding to the body and replace the variable declaration with `$("body").append.`

7. Replace the instances of jquery finds in the show method with references to the instance varaiables, e.g. `this.yesButton`

# showDialog

Now we have extracted the dialog we need a way to render it.

Create a file named `dialogs.js` in the `nmc/dialogs` folder.

1. Import the dialog into `dialogs.js`
   ```javascript
   import ConfirmationDialog from "./dialogs/ConfirmationDialog.js";
   ```
2. Declare a variable

```javascript
let confirmationDialog;
```

3. Add a function to show the dialog

```javascript
/**
 * @description Shows the confirmation window.
 *
 * @param {string} caption - The header of the confirm window.
 * @param {string} confirmMessage - Confirm message.
 * @param {Function} callback - The function to be callled.
 * @param {Function} cancelCallBack - The function to be called when cancel button is clicked.
 * If undefined then it will close the confirmation dialog.
 *
 * @returns {undefined} Nothing.
 */
function showConfirmationWindow(
  caption,
  confirmMessage,
  callback,
  cancelCallBack
) {
  if (!confirmationDialog) {
    confirmationDialog = new ConfirmationDialog();
  }
  confirmationDialog.show(caption, confirmMessage, callback, cancelCallBack);
}
```

add it to the exports

```javascript
export {
  showConfirmationWindow,
```

# Functional test

The devs need a way to confirm the dialogs work as expected. This could be done in the app itself, but that is how QA should test. However, a dev-only way can be provided as a 'smoke' test.

There are three files in the `nmc-lab` project (where this readme file is) under the `StepFive` folder.

1. Copy `DialogTest.js`, `test.ejs` and `test.js` to `src/nmc/dialogs`

2. Copy `dev.webpack.config.js` to the `nmc-client` root.

3. Restart the app server

4. Open a browser to http://localhost:4014/NMCHTML/dialogs.html

5. Add an entry to `launch.json` in the `.vscode` folder
   ```json
   ,
    {
      "type": "chrome",
      "request": "launch",
      "name": "Test dialogs",
      "url": "http://localhost:4010/NMCHTML/dialogs.html",
      "webRoot": "${workspaceFolder}"
    }
   ```
6. Launch that from the debug panel.

The test should fail when clicking the dialog button. Setting a break we see that dialog is undefined. This is because we are in a callback. Change the functions to fat arrow functions.

Note: There is an error indicated in the DEBUG CONSOLE that callback is not a function. That is because one was not provided.

The code in the dialog should check if(callback) before invoking, similarly for cancelcallback

A unit test can now be written to test this.

# Repeat

This process must be repeated for ErrorDialog, ExceptionDialog, InformationDialog. ProgressDialog and SessionTimeoutDialog.js

It seems that confirmDialog is not used. This code can be deleted but a dialog can be created later if needed.

Note that the progress bar will have three methods and the publicNMCprogressWindow function will be changed as:

```javascript
/**
 * @description Manage the state of the progress dialog.
 *
 * @param {string} methodName - The method name.
 * @param {*} param1 - Unkown.
 *
 * @returns {undefined} Nothing.
 */
function publicNMCprogressWindow(methodName, param1) {
  if (methodName === "init") {
    dialogs.showProgressWindow(dialogs.ProgressType.OPEN, param1);
  } else if (methodName === "updateProgress") {
    dialogs.showProgressWindow(dialogs.ProgressType.UPDATE, param1);
  } else if (methodName === "close") {
    dialogs.showProgressWindow(dialogs.ProgressType.CLOSE);
  }
}
```

# JavaScript extensions.

The progress dialog uses a `round` function which is created by the NMCApp.
These JavaScript extensions are added to core JavaScript objects by the NMC app at startup.
Remove the code from the bottom of NMCApp.js and place it into a new file named `javascriptEnhancements.js` in the `src` folder.

add an import for them in `app.js` and in `DialogTests.js`

```javascript
import "./../../JavascriptEnhancements.js";
```

# Session timeout

`SessionTimeoutDialog` should not be made available via `dialogs.js` since it is specific to `NMCApp.js` and is not meant for general consumption.

There are hard coded English literals in the session dialog.

Move the code that starts the timer into the dialog and invoke start in `NMCApp.js`

```javascript
sessionTimeoutDialog.start(NMCInactivityTimeOut);
```

# More functional tests

Write tests for all the dialogs.

# Unit test

copy `informationDialog-test.js` from the `StepFive/all` folder of this project (where this md is)

Review the code

Kill all terminals and browsers (make sure status bar is cyan)

in command prompt type

```
npm install --save-dev sinon-chai jsdom jsdom-global jquery jquery-ui i18next-sync-fs-backend

npm run test-once test/nmc/dialogs/informationDialog-test.js
```
