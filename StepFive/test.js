import i18next from "i18next";
import i18nextXHRBackend from "i18next-xhr-backend";
import { i18nOptions } from "./../../i18n/i18nOptions";

i18next
  .use(i18nextXHRBackend)
  .init(
    i18nOptions,
    /**
     * @description Can receive up to two parameters, (err, t).
     *
     * @example None.
     * @returns {undefined} Nothing.
     */
    function() {}
  )
  .on(
    "initialized",
    /**
     * @description Can receive one parameter, options.
     *
     * We don't want the app to be constructed until the language files are loaded.
     * Therefore we lazy import App which starts the bootstrap process for the UI.
     *
     * No UI components or objects should be imported in this (index.js) file,
     * otherwise that will initiate the bootstrapping.
     *
     * @example None.
     * @returns {undefined} Nothing.
     */
    function() {
      import("./DialogTest").then(Test => {
        new Test.default();
      });
    }
  );
