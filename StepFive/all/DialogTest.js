import "./../../JavascriptEnhancements.js";
import SessionTimeoutDialog from "./SessionTimeoutDialog.js";
import * as dialogs from "./../dialogs.js";

const sessionTimeoutDialog = new SessionTimeoutDialog();

let sessionButton;
let timer;
let progress = 0;

/**
 * @description Build the dialog test page.
 *
 * @returns {undefined} Nothing.
 */
export default function DialogTest() {
  createButton("Confirmation Dialog", showConfirmationDialog);
  createButton("Error Dialog", showErrorDialog);
  createButton("Information Dialog", showInfoDialog);
  createButton("Exception Dialog", showExceptionDialog);
  createButton("Progress Dialog", showProgressDialog);
  sessionButton = createButton(
    "Session Timeout Dialog",
    showSessionTimeoutDialog
  );
}

/**
 * @description Create a button.
 *
 * @param {string} text - The button text.
 * @param {Function} func - The callback function.
 *
 * @returns {HTMLButtonElement} A button.
 */
function createButton(text, func) {
  let button = document.createElement("button");
  button.innerText = text;
  button.onclick = func;
  document.body.appendChild(button);
  return button;
}

/**
 * @description show the InformationDialog.
 *
 * @returns {undefined} Nothing.
 */
function showInfoDialog() {
  dialogs.showInformationWindow("Information Dialog");
}

/**
 * @description show the ConfirmationDialog.
 *
 * @returns {undefined} Nothing.
 */
function showConfirmationDialog() {
  dialogs.showConfirmationWindow("Confirmation Dialog");
}

/**
 * @description show the ErrorDialog.
 *
 * @returns {undefined} Nothing.
 */
function showErrorDialog() {
  dialogs.showErrorWindow("Error Dialog");
}

/**
 * @description show the ExceptionDialog.
 *
 * @returns {undefined} Nothing.
 */
function showExceptionDialog() {
  dialogs.showExceptionWindow({
    name: "Exception name",
    message: "Exception message"
  });
}

/**
 * @description show the ProgressDialog.
 *
 * @returns {undefined} Nothing.
 */
function showProgressDialog() {
  dialogs.showProgressWindow(dialogs.ProgressType.OPEN, 100);
  timer = setInterval(increment, 1000);
}

/**
 * @description Increment the progress timer.
 *
 * @returns {undefined} Nothing.
 */
function increment() {
  progress += 10;
  if (progress > 100) {
    clearInterval(timer);
    dialogs.showProgressWindow(dialogs.ProgressType.CLOSE);
    return;
  }

  dialogs.showProgressWindow(dialogs.ProgressType.UPDATE, progress);
}

/**
 * @description show the SessionTimeoutDialog.
 *
 * @returns {undefined} Nothing.
 */
function showSessionTimeoutDialog() {
  sessionButton.disabled = true;
  sessionTimeoutDialog.start(1);
}
