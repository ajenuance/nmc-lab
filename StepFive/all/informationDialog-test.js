import chai, { expect } from "chai";
import sinon from "sinon";
import sinonChai from "sinon-chai";

import i18next from "i18next";
import i18nextSyncBackend from "i18next-sync-fs-backend";
import InformationDialog from "../../../src/nmc/dialogs/InformationDialog.js";

import { JSDOM } from "jsdom";
import jQuery from "jquery";

chai.should();
chai.use(sinonChai);

const DIALOG_MESSAGE = "Dialog message";

var dom = new JSDOM("<!DOCTYPE html>");
global.document = dom.window.document;
global.window = dom.window;
let $ = jQuery(dom.window);
global.$ = $;
global.jQuery = $;
global.window.jQuery = $;
global.navigator = {
  userAgent: "node.js"
};

require("jquery-migrate");
require("jquery-ui/ui/widget.js");
require("jquery-ui/ui/unique-id.js");
require("jquery-ui/ui/widgets/dialog.js");
require("jquery-ui/ui/widgets/button.js");
require("jquery-ui/ui/safe-active-element.js");
require("jquery-ui/ui/data.js");
require("jquery-ui/ui/tabbable.js");
require("jquery-ui/ui/focusable.js");

let i18nOptions = {
  initImmediate: false,
  fallbackLng: "en",
  returnObjects: true,
  debug: false,
  ns: ["framework", "platform"],
  backend: {
    loadPath: "./locales/{{lng}}/{{ns}}.json",
    crossDomain: true
  },
  interpolation: {
    /**
     * @description Description.
     *
     * @param {string} value - The value to be formatted.
     * @param {string} format - The regex format.
     *
     * @returns {undefined} Nothing.
     */
    format: function(value, format) {
      if (format === "uppercase") {
        return value.toUpperCase();
      }
      return value;
    }
  }
};

describe("InformationDialog tests", function() {
  before(() => {
    i18nOptions.initImmediate = false;
    i18next.use(i18nextSyncBackend).init(i18nOptions);
  });

  it("Invoking show sets the dialog message", () => {
    let dialog = new InformationDialog();
    let message_spy = sinon.spy(dialog.message, "text");

    dialog.show(DIALOG_MESSAGE);

    expect(message_spy).to.have.been.calledWith(DIALOG_MESSAGE);
  });

  it("Invoking show opens the dialog", () => {
    let dialog = new InformationDialog();
    let dialogSpy = sinon.spy(dialog.dialog, "dialog");

    dialog.show(DIALOG_MESSAGE);

    expect(dialogSpy).to.have.been.calledWith("open");
  });

  // This is done last to avoid the cleanup
  xit("Dialog HTML is correct", () => {
    let $ = sinon.stub();
    global.$ = $;

    let bodyStub = sinon.stub({
      /** @description A stub to detect invocations of append.
       *  @returns {undefined} Nothing.
       */
      append: function() {},
      /**
       * @description mock out the dialog function.
       * @returns {undefined} Nothing.
       */
      dialog: function() {},
      /**
       * @description mock out the dialog function.
       * @returns {undefined} Nothing.
       */
      on: function() {}
    });
    $.withArgs("body").returns(bodyStub);
    $.withArgs(sinon.match(/#.*/)).returns(bodyStub);

    new InformationDialog();

    const html = `<div
        id="div_19C5FFC4E3A1495086CAFA60F93C5F4D"
        title="Nuance Management Center"
        style="display:none;min-height:40px !important;">
        <div>
          <label id="txt_3E9A9BC484A1478197BDAF9AD6F05092"></label>
        </div>
        <div style="width:100%;margin-top:20px;text-align:center;">
          <button
            id="btn_F45841F311AB4172A47380F136DE5C1C"
            class="nmc-button"
            style="margin:0px 0 30px 10px;">OK</button>
        </div>
      </div>`;

    sinon.assert.calledWith(bodyStub.append, html);
  });
});
