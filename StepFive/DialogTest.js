import * as dialogs from "./../dialogs.js";

/**
 * @description Build the dialog test page.
 *
 * @returns {undefined} Nothing.
 */
export default function DialogTest() {
  createButton("Confirmation Dialog", showConfirmationDialog);
}

/**
 * @description Create a button.
 *
 * @param {string} text - The button text.
 * @param {Function} func - The callback function.
 *
 * @returns {HTMLButtonElement} A button.
 */
function createButton(text, func) {
  let button = document.createElement("button");
  button.innerText = text;
  button.onclick = func;
  document.body.appendChild(button);
  return button;
}

/**
 * @description show the ConfirmationDialog.
 *
 * @returns {undefined} Nothing.
 */
function showConfirmationDialog() {
  dialogs.showConfirmationWindow("Confirmation Dialog");
}
