const merge = require("webpack-merge");
const baseConfig = require("./base.webpack.config.js");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = merge(baseConfig, {
  mode: "development",
  devtool: "eval-source-map",
  entry: {
    dialogs: "./src/nmc/dialogs/test.js"
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Dialog test page",
      template: "!!ejs-compiled-loader!./src/nmc/dialogs/test.ejs",
      minify: {
        collapseWhitespace: true
      },
      hash: false,
      filename: "./dialogs.html",
      chunks: {
        dialogs: {
          entry: "test.js"
        }
      },
      excludeChunks: ["index"]
    })
  ]
});
