This is actually used when invoking NMCApp.fireEvent, which uses publicPubSub ... which should be a separate module and so cancleEvent should probably be in that.

For now, define it as a class.

```javascript
/**
 * @description Cancel event class.
 *
 * @returns {Object} An object with references to the function defined by CanceleventArgs.
 */
export default class CancelEventArgs {
  /**
   * @description Create a new CanceleventArgs.
   */
  constructor() {
    this.isCancelled = false;
  }

  /**
   * @description Test whether the event should be cancelled.
   *
   * @returns {boolean} True if the event should be cancelled.
   */
  isCancelled() {
    return this.isCancelled;
  }

  /**
   * @description Cancels the event.
   *
   * @returns {undefined} Nothing.
   */
  cancel() {
    this.isCancelled = true;
  }
}
```

TODO: Consider making this an object in `PubSub.js` when that is created.
