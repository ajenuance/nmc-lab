Now that session info can be retrieved the code from `appScripts.ejs` can be moved into `app.js`.

The session data must first be retrieved. The `axios` module is used for this.

```
npm install axios
```

Add an import into `app.js`. By convention imports for modules are generally coded before project imports, but it is not required.

```javascript
import axios from "axios";
```

Add the following code after the assignment to `window.onunload` to invoke the api call:

```javascript
axios.get("SessionInfo").then(json => this.startSession(json));
```

Add the function after the constructor:

```javascript
  startSession(json) {
    SysErr.logMessage(`User ${json.lastName}, ${json.firstName} - ${json.UserId} logged in.`);
  }
```

Note. Now that the session info is available the code in `main.cshtml` indicating login user has be added.

However, this causes an error since `SysErr.js` has not been imported.

Move SysErr.js from the `NMCHTML/Scripts` folder to `src/nmc`

See `SysErr.md`
